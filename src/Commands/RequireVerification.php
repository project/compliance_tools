<?php

namespace Drupal\compliance_tools\Commands;

use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Drush command to return the configuration for email verification.
 */
class RequireVerification extends DrushCommands {

/**
    * The config factory service.
    *
    * @var \Drupal\Core\Config\ConfigFactoryInterface
    */
   protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.settings'),
    );
  }

  /**
   * Display the setting for require email verification.
   *
   * This commands validates NIST 800-53 control IA-4 (b).
   *
   * @command ct:email-verification
   * @aliases ct-ev
   * @usage ct:email-verification
   *   Get the setting for require email verification when a visitor creates an
   *   account.
   */
  public function getEmailVerification() {
    $setting = $this->configFactory->get('verify_mail');
    $verified = ($setting) ? 'true' : 'false';
    $this->output()->writeln('Email verification enabled: ' . $verified);
  }

}
