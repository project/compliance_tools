<?php

namespace Drupal\compliance_tools\Commands;

use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;


/**
 * Drush command to return the configuration for failed login attempts.
 */
class GetFailedLogins extends DrushCommands {

     /**
    * The config factory service.
    *
    * @var \Drupal\Core\Config\ConfigFactoryInterface
    */
   protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.flood'),
    );
  }

  /**
   * Display the settings for failed logins and lockout window.
   *
   * This commands validates NIST 800-53 control AC-7 (a) and AC-7 (b) and
   * SC-5.
   *
   * @command ct:failed-logins
   * @aliases ct-fl
   * @usage ct:failed-logins
   *   Get the settings for failed logins.
   */
  public function getLoginAttempts() {
    $loginAttempts = $this->configFactory->get('user_limit');
    $this->output()->writeln('Failed logins: ' . $loginAttempts);
    $loginWindow = $this->configFactory->get('user_window');
    $loginWindowTime = $loginWindow / 60;
    $this->output()->writeln('Lockout window: ' . $loginWindowTime . ' minutes');
    $ipAttempts = $this->configFactory->get('ip_limit');
    $this->output()->writeln('IP failed logins: ' . $ipAttempts);
    $ipWindow = $this->configFactory->get('ip_window');
    $ipWindowTime = $ipWindow / 60;
    $this->output()->writeln('IP lockout window: ' . $ipWindowTime . ' minutes');
  }

}
